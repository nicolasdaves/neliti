from django.http import HttpResponse
from .models import Journal, Hit
import json

# Create your views here.

def index(request):
    return HttpResponse(json.dumps(get_journal_statistics()))

def get_journal_statistics():
    # Construct summary dict in the form {journal_id -> (total_views, total_downloads)}
    # Declare empty dictionary 
    summary = dict()
    
    # Get all Journals with related publications and hits
    journals = Journal.objects.prefetch_related('publication_set__hit_set').all()   
   
    # Iterate journals 
    for journal in journals:
        # Initialize variables
        pageviews = 0
        downloads = 0

        # Get all publications of current journal
        pubs_of_journal = journal.publication_set.all()

        # Iterate publications of this journal
        for pub in pubs_of_journal:

            # Get all hits of this publication
            hits_of_pub = pub.hit_set.all() 

            # Calculate pageviews and downloads
            for hit in hits_of_pub:
                if hit.action == Hit.PAGEVIEW:
                    pageviews += 1
                else:
                    downloads += 1

        # Assign results to dictionary
        summary[journal.id] = (pageviews, downloads)
    
    return summary
    

