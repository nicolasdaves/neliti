from django.http import HttpResponse
from django.shortcuts import render
from django.template import loader
import requests

# Create your views here.
def index(request):
    # initialize coordinates
    lat = '53'
    lon = '-0.25'

    # get coordinates from url
    if 'lat' in request.GET: 
        lat = request.GET['lat']

    if 'lon' in request.GET: 
        lon = request.GET['lon']

    # weather API url
    url = 'https://api.met.no/weatherapi/locationforecast/2.0/compact?lat={}&lon={}'

    # load template 
    template = loader.get_template('index.html')

    # add http header to solve 403 error
    headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.0.0 Safari/537.36'}
    res = requests.get(url.format(lat, lon), headers=headers).json()

    print(res)
    print(url.format(lat, lon))

    # context for template
    context = {
        'lon': res['geometry']['coordinates'][0],
        'lat': res['geometry']['coordinates'][1],
        'unit': res['properties']['meta']['units']['cloud_area_fraction'],
        'fraction': res['properties']['timeseries'][0]['data']['instant']['details']['cloud_area_fraction'],
    }

    return HttpResponse(template.render(context, request))